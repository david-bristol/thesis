\chapter{Introduction}

A folklore saying in computer science, often attributed to Dijkstra, goes that ``Computer science is not about computers, any more than surgery is about knives or astronomy is about telescopes''.
This, then, is not a thesis about encryption.
If we had to settle on a single term to capture the essence of modern cryptography, the closest we can think of is ``trust''.
On the one hand, cryptographic tools such as encryption or digital signatures aim to create trust --- trust that a message cannot be read or tampered with by others; trust that a person really is who they say they are; trust that I am sending my credit card number to the shop I am trying to do business with and not to a fraudster.

Trust is a social concept, not a physical one.
Nonetheless, physical items play their part in establishing and maintaining trust between humans, for example opaque paper envelopes in sealed-bid auctions, signatures on dotted lines or candle-wax seals.
What these tools and techniques strive for in the physical realm, cryptography aims to replicate in the world of information.
Information does not behave like an everyday object --- it does not exist at a point in space and time and can in principle be freely duplicated but is notoriously hard to destroy --- yet the existence and utility of information are not only beyond doubt but central to the society we live in.

The properties of information, as opposed to those of physical objects, allow cryptography to propose solutions that at a first glance look like contradictions.
In Chapter \ref{c:voting} on voting, we analyse a system that is designed to not let anyone know anyone else's vote, yet at the same time everyone can check that all votes are counted correctly.
One of the key tools in this process, which forms the main topic of this thesis, is known as a zero-knowledge proof scheme.
Such a scheme is a recipe for two mutually distrustful parties to cooperate and achieve a common aim, without either of the two being able to cheat the other.
The closest physical analogue of a zero-knowledge protocol is perhaps the ``cut and choose'' technique for two children to divide up a cake: 

\needspace{3\baselineskip}
\begin{definition}[``Cut and choose'' cake protocol]
\ 

\begin{enumerate}
\item The first child cuts the cake into two pieces.
\item The second child selects a piece, leaving the remaining one for the first child.
\end{enumerate}
\end{definition}

The ``security notion'' achieved by this protocol is that neither child will have reason to complain that their piece of cake is smaller than the other piece.
The principle of ``cut and choose'' is in fact used directly in sophisticated cryptographic protocols.

If cryptography is supposed to create trust, do we first have to trust cryptography?
Since we started writing this thesis, revelations in the media have confirmed not only the obvious --- that our spies engage in spying --- but that there have been attempts to subvert cryptographic protocols and standards to leave open a ``back door'' for our intelligence agencies.
Yet many people face a far bigger hurdle to trusting cryptography than possible compromise by our intelligence agencies: modern cryptography is based on ``pure'' mathematics such as number theory, inaccessible to a majority of the human population.
While cryptography comes into play every time we withdraw money from an ATM, make a call on a mobile phone or visit all but the most basic of websites, the trust barrier to adopting cryptographic techniques in nationwide elections is high and rightly so.

One could be tempted to say that at least within the cryptographic community, one should publish every construction in full detail, develop mathematical security models and assumptions and verify each other's work in a spirit of constructive criticism.
Yet we then face a different problem, as Halevi famously stated:

\begin{quote}
\ldots as a community, we generate more proofs than we carefully verify (and as a consequence some of our published proofs are incorrect). \hfill\cite{H05}
\end{quote}

In the same year, Snow from the NSA pointed out a less academic view of the matter:

\begin{quote}
We do not ``beta-test'' on the customer; if my product fails, someone might die. \hfill\cite{S05}
\end{quote}

There has been much debate on the role of security proofs in cryptography and their relation (or lack thereof) to practice.
For us, a big concern is that cryptographic security proofs often consist of much drudgery in exchange for very little insight.
As a consequence, not only are our proofs not verified carefully but occasionally also written with a certain amount of hand-waving and imprecision.
%The topic of interest after all is the new concept or scheme one has developed and its potential applications; the security proof is tucked away in the appendix which is not required reading for conference reviewers.
In our previously published work, both on Direct Anonymous Attestation (DAA) and on voting, we have analysed schemes that were thought or even ``proven'' to be secure.
In both cases we found otherwise.
The problems in both cases were related to the use and modelling of zero-knowledge proof schemes.
By mathematically analysing components of these schemes at a greater level of detail than had been done previously, we uncovered issues that had been omitted in earlier work.

%%Contrast this with the modus operandi in mathematics, where the insight offered by a proof is often of far greater interest than the statement proved itself and no paper gets published without a thorough review of all claims.
%%%There, a new proof of an old theorem that adds extra understanding of the subject matter is often preferred over a proof of a new result that offers few further insights --- mathematicians aim for their proofs to be anything but zero-knowledge!
%%This is not to suggest that mathematicians do not make mistakes.
%%What we do suggest is that there are factors at play within the discipline of cryptography that, if reduced or eliminated, would lessen the chance that we do let mistakes slip through unnoticed.
%%One such factor is that proof-checking is all but absent in our current peer-review process.

We suggest that cryptographers must not only consider the process by which the community verifies security arguments but also actively seek to make these arguments as simple as possible to write and verify.
We see one of the future challenges for cryptography in designing models and methods by which we can reduce the painstaking and time-consuming processes of proof creation and verification or better still, automate these processes.

The discipline known as formal methods (which Snow \cite{S05} also suggested in passing) offers some hope of automating mundane proof-verification tasks; more recently tools such as EasyCrypt \cite{ec} have appeared that aim to automate verification of a particular style of security analysis widely used in cryptography and in this thesis in particular, so-called game-based security.
However, the tools available for automated analysis at the moment have a steep learning curve and require much knowledge and experience to use.
We are not yet at the point where we could apply such tools to our own work although we consider this a valuable direction to pursue in the future.
For now, we choose in this thesis to adopt an intermediate step, giving all security analysis in the code-based game-playing framework of Bellare and Rogaway \cite{BR06} which aims for precision and the possibility of automation.

We are not yet satisfied with any approach for reasoning about zero-knowledge proofs including the one in this thesis, which is at most a passable approximation.
One of our problems is that we rely on techniques that work well for reasoning about two-party systems yet the security models for proof schemes include further, hypothetical parties such as ``simulators'' and ``extractors''.
In any case, we believe that there is much interesting future work in the area of zero-knowledge.

\section{Publications}
The following is an overview of our publications and their relation to this thesis.
We analysed direct anonymous attestation (DAA), contributing to a new security model and fixing an earlier, broken scheme together with Fuchsbauer, Ghadafi, Smart and Warinschi \cite{BFGSW11} in a work published in the International Journal of Information Security. 
Later, we assisted Ghadafi and Fuchsbauer \cite{BFG13} in constructing the first DAA scheme that does not require random oracles (to be introduced in Section \ref{s:rom}) in work published at the International Conference on Applied Cryptography and Network Security (ACNS).

This thesis is not concerned with DAA. However, some ideas on security models in general and the flaws that we and others discovered a previously proposed and supposedly ``proven secure'' DAA scheme helped influence us to choose the code-based game-playing framework of Bellare and Rogaway as a basis for the security arguments in this thesis, hoping to avoid such problems.
We introduce this framework formally in Section \ref{s:cbg}.

Our work on zero-knowledge proofs started with our analysis of the Helios voting system together with Cortier, Pereira, Smyth and Warinschi \cite{BCPSW11} that we published at Esorics.
We develop the ideas of this paper further in Chapter \ref{c:voting} of this thesis.
We aimed to prove that Helios satisfies a property that we call ballot privacy (our Definition \ref{def:bp}) if ``done properly'', since Cortier and Smyth had found some potential attacks on Helios.
In a later work available on eprint \cite{BPW12a}, we weakened the conditions under which we could show ballot privacy.
In this thesis, we show ballot privacy under even weaker preconditions, yielding the first proof to consider full Helios ballots rather than individual ciphertexts (Theorem \ref{thm:helios}).
As part of our analysis of Helios, we needed to establish that the encryption used in Helios satisfies a security property known as non-malleability (addressed in Section \ref{s:nm}).
Based on the existing literature, we assumed this was obvious --- but it turned out to be incorrect once we analysed a certain construction known as Encrypt+PoK (Section \ref{s:encpok} in this thesis) in detail.

The problem with Helios was that it used a technique that we called the weak Fiat-Shamir transformation (discussed in Section \ref{s:wfs}) which turns out to be problematic.
We wrote a paper on this transformation and its pitfalls with Pereira and Warinschi \cite{BPW12b} which we published at Asiacrypt.
In this thesis, we build on the results of the Asiacrypt paper in Section \ref{s:strong}.

As a spin-off from our main line of work on zero-knowledge proofs, we compared our notion of ballot privacy to other ideas in the literature.
With Cortier, Pereira and Warinschi \cite{BCPW12} we defined a notion of privacy based on conditional entropy and showed that our original notion implies the entropy-based one; we published this work at ACM-CCS.
With Smyth \cite{BS13}, we compared privacy to ballot independence, formalising the notions and showing their equivalence for ``Helios-like'' schemes.
This work, like our original paper on Helios, was also published at Esorics.
Independently, with Neumann and Volkamer \cite{BNV13} we studied the practical applicability of so-called malleable proofs and published a paper at Vote-ID.
We do not discuss the works cited in this paragraph any further in this thesis.

Shifting our focus from Helios to the zero-knowledge proofs themselves, we began to explore the different strengths in which these proofs come.
When one uses such proofs in conjunction with encryption schemes then there is an interesting correspondence between security levels for encryption and proofs.
We had shown in our Asiacrypt paper that the well-known ``signed ElGamal'' scheme is non-malleable (a ``folklore'' result, for which we could find no previous proof however).
Yet it was an open problem, to our knowledge first asked by Shoup and Gennaro in 1998, whether this scheme also meets the stronger notion of chosen-ciphertext security (our Definition \ref{def:ind}).
With Fischlin and Warinschi, we defined a notion of multi-proofs (Definition \ref{def:mpok}) with which we can provably achieve chosen-ciphertext security (Theorem \ref{thm:cca}).
Using a technique known as meta-reductions, we further showed that proofs based on the Fiat-Shamir transformation (Section \ref{s:fs}) such as used in Helios are not multi-proofs (Theorem \ref{thm:not-mpok}).
By adapting both the techniques in this proof and the assumptions of the theorem, we finally showed that under plausible conditions, the encryption scheme under consideration is not chosen-ciphertext secure (Theorem \ref{thm:not-cca}), giving a negative answer to a long-standing open problem.
The work referenced in this paragraph is currently in submission to Eurocrypt and as such, unpublished.
We reproduce a large part of this work in Chapter \ref{c:pok2} of this thesis, albeit with a slightly different presentation founded on code-based game-playing.

We give a complete list of our publications to date and their key topics in the following table.

\begin{center}
\begin{tabular}{lll}
\toprule
\htext{reference} & \htext{published} & \htext{topics} \\
\midrule
\cite{BFGSW11} & IJIS & DAA security models \\
\cite{BCPSW11} & Esorics & ballot privacy definition, proof using CCA \\
\cite{BPW12a} & eprint & ballot privacy from non-malleable encryption \\
\cite{BPW12b} & Asiacrypt & weak proofs and attacks on Helios \\
\cite{BCPW12} & ACM CCS & entropy-based notion of ballot privacy \\
\cite{BFG13} & ACNS & DAA without random oracles \\
\cite{BS13} & Esorics & relationship between ballot privacy and independence \\
\cite{BNV13} & Vote-ID & practicality of mixnets based on malleable proofs \\
\bottomrule
\end{tabular}
\end{center}
{\scriptsize IJIS is the International Journal of Information Security; ACM CCS is the Association for Computing Machinery (conference on) Computer and Communications Security and ACNS is the (International Conference on) Applied Cryptography and Network Security.}

\section{Outline of this thesis}
In Chapter \ref{c:pre} we introduce the mathematical tools required to reason about the security of cryptographic schemes.
We begin by introducing concepts from algebra and complexity theory that appear throughout cryptography before we introduce game-based security starting from Section \ref{s:herebeginscrypto}.
We define the code-based game-playing framework in Section \ref{s:cbg}
and give a formal definition of a game in this framework (Definition \ref{def:game}) which we use in the rest of this thesis.
In Chapter \ref{c:enc} we discuss public-key encryption and its security levels, the cornerstone being Definition \ref{def:ind}.
Much of our work on proof schemes is motivated by the desire to understand how they can interact with public-key encryption and how this affects these security levels.
In Chapter \ref{c:zk} we introduce proof schemes and the main security notions such as zero-knowledge (Definition \ref{def:zk}) and proof of knowledge (Definition \ref{def:pok}).

Our main contributions are in Chapter \ref{c:pok2}. 
We first distinguish weak and strong proofs and highlight the weaknesses of weak proofs.
Next, we introduce multi-proofs (informally: ``even stronger proofs'') in Definition \ref{def:mpok} and prove a separation between strong and multi-proofs in Theorem \ref{thm:not-mpok}.
Along the way we apply these concepts to the Encrypt+PoK construction built upon ElGamal encryption and see that there is a correspondence between weak proofs and chosen-plaintext attacks, strong proofs and non-malleable encryption (Theorem \ref{thm:nm}) and multi-proofs and chosen-ciphertext attacks (Theorem \ref{thm:cca}).
We further prove that strong proofs alone are insufficient to achieve chosen-ciphertext security in Theorem \ref{thm:not-cca}.

Finally, in Chapter \ref{c:voting} we define ballot privacy for voting and apply the theory from the previous chapters to show that Helios, with strong proofs, meets our notion.
We summarise the main results of this thesis in the following table.

\begin{center}
\begin{tabular}{ll}
\toprule
\htext{item} & \htext{summary} \\
\midrule
Section \ref{s:wfs} & Problems with weak Fiat-Shamir. \\
Definition \ref{def:spok} & Strong proofs. \\
Theorem \ref{thm:sse} & Strong Fiat-Shamir is strong and simulation sound.\\
Definition \ref{def:mpok} & Multi-proofs. \\
Theorem \ref{thm:not-mpok} & Fiat-Shamir-Schnorr is not a multi-proof. \\
Theorem \ref{thm:cca} & Encrypt+multi-PoK yields \notion{CCA} security. \\
Theorem \ref{thm:not-cca} & No reduction can show \notion{CCA} security of Signed ElGamal. \\
Definition \ref{def:bp} & Ballot privacy for voting systems. \\
Theorem \ref{thm:helios} & Helios (with strong proofs) satisfies ballot privacy. \\
\bottomrule
\end{tabular}
\end{center}


