David's PhD thesis
==================

1. Introduction

    Very high level overview of what crypto in general and this thesis in particular are about.

2. Non-cryptographic preliminaries

    Idea: introduce all notions that crypto "borrows" from other areas,
    give a brief description at the end of each how it's used in crypto.

    - Terminology and notation
    
    - Group theory
    
        Group, ring, field,
        group exponentiation and dlog.
        
    - Complexity theory
    
        efficiency, NP, reductions ...
    
    - Software engineering 
    
        encapsulation/black box, interface vs. implementation, 
        implementing an interface given another
        
3. Cryptographic preliminaries

    - ways of defining security: games, simulation, DY
    
    - overview of game-based security as this is the approach we use
    
    - overview of code-based game playing
    
    - environment models: RO, CRS
    
    - (prehaps a short rant about "another look")
    
    - common computational assumptions
    
4. Public-Key Encryption

    - overview/history

    - definition of PKE scheme
    
    - IND security (big game that covers CPA, 1-CCA, CCA1, CCA2)
    
    - Example: ElGamal; security proof
    
    - NM 
        * intuition (sem. sec. wrt. relations)
        * game
        * Bellare-Sahai
        * [CONTRIB] NM mod relation
    
    - Relations among security notions (brief overview)
    
    - Homomorphic encryption
    
        * Definition
        * Submission security, [CONTRIB] "verifiable augmentation"
        
            or, why homomorphic and non-malleable don't really exclude
            each other, [CONTRIB] application to voting
    
        * Example: Cramer-Shoup
        
    - Threshold encryption
    
          * definition
          * application to ElGamal
          * [CONTRIB] problems with implementation - example: BBS/DLIN

5. ZK proofs

    - Overview/history
    
    - proof schemes
    
    - security properties (existing)
    
        ZK, soundness, PoK, interactive/non-interactive cases
        
    - sigma protocols
    
        and the Fiat-Shamir transformation
        
    - [CONTRIB] Strong proofs
    
        weak Fiat-Shamir and problems (with applications), strong FS 
        and proof that sigma protocols actually give this
    
    - [CONTRIB] multi-proofs
    
        Note: multi-proofs could go into their own chapter
        
        motivation, definition, example: Fischlin, 
        metareduction-separation on FSS
    
    - [CONTRIB] Encrypt + PoK
    
        * levels of security: wFS+CPA=CPA, sFS+CPA=NM, mPoK+CPA=CCA,
        
        *(slPoK+CPA = PA >= CCA ?)
        
        * metareduction
        
        * Examples: NY, CP-SS-ElG
    
    - Examples of further real-world applications of ZK proofs 
      (beside Enc+PoK and Helios).
      
          Could include [CONTRIB] something about DAA, mention MS-ZQL
          (which I may have more to say on after my internship).
    
    - outlook/future work: when the relation is only "pseudo" NP
    
        Examples: TDH?
    
6. [CONTRIB] Helios

    - introduction to voting
    
        Overview of points made by Adida, Benaloh and others.
        Notions such as IV/UV/EV/BV, intended-cast-recorded-tallied etc.
    
    - SPS model
    
    - ballot privacy
    
        probably just go with the eprint/esorics model and leave the
        alpha/beta bit for now
    
    - minivoting and proof
    
    - ballot privacy proof for Helios
    
    - future work: verifiability
