\chapter{Conclusion}

We have studied the theory of zero-knowledge proof schemes and some practical applications in the Helios voting system.
Our first contribution was to identify weak proofs (Definition \ref{def:wfs}) and problems that arise from their use.
Next, we formalised strong proofs (Definition \ref{def:spok}), showed how they could be obtained (Theorem \ref{thm:sfs}) and proved that they really do strengthen encryption when deployed in the common Encrypt+PoK construction (Theorem \ref{thm:nm}).

In 1998, Shoup and Gennaro \cite{SG98} discovered an apparent limitation of sigma protocol-based proof schemes: an ``obvious'' proof of chosen-ciphertext security for a scheme known as Signed ElGamal (our Definition \ref{def:seg}) breaks down when one tries to write it out in detail.
Shoup and Gennaro were unable to either restore the proof by other means nor to prove that this construction fails to achieve the desired notion, leaving the problem as an open question.
In this thesis, we have answered this question negatively.
We have introduced a notion of multi-proofs (Definitions \ref{def:mpok},\ref{def:ssmpok}) and shown that the ``obvious'' proof of chosen-ciphertext security does hold in the presence of simulation sound multi-proofs (Theorem \ref{thm:cca}).
The proof is anything but obvious however: it is to our knowledge the first proof of chosen-ciphertext security using schemes that rely on a technique known as rewinding, the very technique that caused problems for Shoup and Gennaro.
To complete our work on the theory of zero-knowledge proofs, we have shown two separation results.
First, we have proved that sigma-protocol based schemes (whether weak or strong) cannot be multi-proofs (Theorem \ref{thm:not-mpok}).
We have introduced an assumption that we call IES (Assumption \ref{a:ies}), failure of which would raise serious questions about the security of the Signed ElGamal scheme.
Under this assumption, our second separation theorem (Theorem \ref{thm:not-cca}) is that Signed ElGamal cannot be proven chosen-ciphertext secure, answering the open question to our satisfaction.

We close by taking a step back to observe the big picture and a step forward to consider possible future work.
Looking at the big picture of ``provable security'', we believe that there are several attributes of our field of research in need of improvement.
We have a jungle of methods (game-based and simulation-based security, Dolev-Yao etc.), models (CRS, random oracle etc.) and assumptions (discrete logarithms, Diffie-Hellman etc.).
Particularly when dealing with zero-knowledge schemes, unified definitions are still lacking that capture the essence of the notion at hand but are as far as possible agnostic of a particular model.
At the same time, the tedious low-level work that the different models and methods require can tempt cryptographers to gloss over steps in proofs or settle for ``proof sketches'', increasing the chance of mistakes.

We have started to search for a solution to these issues in this thesis and will continue on this path in the coming years.
We have, for example, presented our definitions of a strong proof (Definition \ref{def:spok}) and multi-proof (Definition \ref{def:mpok}) first in a more model-independent manner and then, for the rewinding random oracle model, spelled them out in detail using code-based games.
Our hope in future work is to formulate the models in such a way that the model-specific definitions become functions of the generic definition and a model description.

To lessen the chance of mistakes in proofs, we have high hopes for automated proof-verification techniques and hope to study such techniques in the near future and if feasible, extend them to cover the applications that we require.
A key problem here will be how to deal with rewinding, which turns a two-party system into a multi-party one.

Another problem that appears regularly concerns addressing conventions in (token-based) concurrent systems.
Some models based on universal composability introduce elaborate conventions here.
We appreciate their precision but would hope for a method to eliminate as many such details as possible --- without sacrificing rigour, of course --- especially when these addressing conventions in the security argument do not have a counterpart in the real scheme under consideration.
Considering the amount of research effort already spent in this area however, we do not have any hope of a quick solution.   

Finally, while we have established not only the notion of multi-proofs but also found a scheme (by Fischlin \cite{F05}) that meets this notion, it is still an open question whether there exist useful multi-proofs with some amount of inherent rewinding (the Fischlin scheme is ``straight line''), or whether Fiat-Shamir-Schnorr could be sensibly extended to become a multi-proof.
Since we know that multi-proofs can be used to yield \notion{CCA} encryption, we have also asked ourselves whether existing \notion{CCA} schemes (Naor-Yung \cite{NY90}, TDH2 \cite{SG98} and Chaum-Pedersen Signed ElGamal \cite{ST13}) can be viewed as Encrypt+multi-proof instances.
At a first glance, this seems to be the case but the statements being proved in this case depend on extra keys and so are not in $\mathbf{NP}$.
This breaks our current theory since a reduction that verifies such proofs would not be efficient.
We hope to extend the theory to be able to handle these cases and uncover the ``essence'' of chosen-ciphertext security in these schemes.

