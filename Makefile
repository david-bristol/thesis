PDF: *.tex
	pdflatex --synctex=1 --halt-on-error --interaction=nonstopmode thesis.tex

MK: *.tex
	latexmk -pdflatex='pdflatex --synctex=1' -pdf thesis.tex

